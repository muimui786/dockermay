describe('My First Test', () => {
    beforeEach (() => {
        Cypress.Cookies.preserveOnce('cookieconsent_status')
      });

    it(`open home page`, () => {
        cy.visit('https://www.rapp.com/', {timeout: 10000});
        cy.get('#accept',{timeout: 10000}).click();
    });
    
    it(`Go to careers section`, () => {
        cy.get('#link_nav_work').click();
        //cy.get('#accept',{timeout: 10000}).click();
        cy.wait(5000)
    });
    
    it(`Take a screenshot`, () => {
        cy.screenshot();
    });
})



